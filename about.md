---
layout: page
title: About
permalink: /about/
---

A Glass of Lores is a game that is still in development. It's inspired by great Japanese SNES-Era  RPG games that I've played on my childhood.

The game started as a demo for a RPG engine that I'm making in javascript and evolved. I plan to release fully documented source on everything, but keep the assets for the game.

The engine runs from human readable code, that can be written in simple text editor or in my custom game builder that I plan to release after - it's still too alpha.

All the game is being made in my free time, since I work a (more than) full time job, this means that I can't make promises regarding development time, features and things like that. I'm making the game all alone, so if you want to contribute, with art, writing, level design, programming, hit me on twitter and we can work from there!

If you liked the design of the blog, the code is not mine, it's the Mediator Jekyll Theme, by Dirk Fabisch, which can be downloaded here:  [https://github.com/dirkfabisch/mediator](https://github.com/dirkfabisch/mediator)

Oh, and ideally I would like to build a good game, but right now along with the game design, the writing, I'm also working in the programming, making the assets, images and music. But this all takes time, and I'm not very good on most, specially the arts. I'm leaving a donate button below, as an experiment. Any money I gather there I will use to get professionals to draw tiles, sprites, compose music and even rewrite the story.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="6JNE55N263FY4">
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif" width="1" height="1">
</form>

Se você for brasileiro, o PayPal bloqueia doações em dólares originárias de brasileiros para brasileiros. Use o botão abaixo nesse caso:

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="HREE83D8BVK2L">
<input type="image" src="https://www.paypalobjects.com/pt_BR/BR/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - A maneira fácil e segura de enviar pagamentos online!">
<img alt="" border="0" src="https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif" width="1" height="1">
</form>
