---
layout: post
title:  "Mean While in the Forest"
date:   2015-02-17 23:34:25
categories: feature
tags: featured demo
image: /assets/article_images/2015-02-17-mean_while_in_the_forest/bgpost2.JPG
---
#In another place of the island
<i>An unexpected storm. Our ship was lost. We were floating on some piece of the ship’s hull, my husband was unconscious, but still alive. Where the hell that came from? At the time, some weird mist engulfed us, and then, we hit the shore. Your dad was very heavy, and very wet. The beach we’ve reached was inside a cave. I remember it as a beautiful place, it was night at the time, but there was a light in the cave, but I don’t remember any kind of fire, no torches.</i>

<i>As I said, your dad was very heavy, I was scared, I don’t know from where I got the strength to carry him for so long. I remember little after getting out of the cave, but I know there were trees, and then I couldn’t take anymore. I dropped your father on the ground, laid his head on the soft grass and then I fell, exhausted, asleep.</i>

<i>On the next morning, I woke, the sun was shining, and I knew that before opening my eyes. My vision failed a little at the time, since I left the darker of my dreams, and now I was in a beautiful forest, butterflies were flying, and we were in the middle of a clearing. A bird, intensively blue, landed on your dad’s face. I got terrified, my mind is getting blurred, lost, “don’t you leave me here!”. Then he sneezed. I never laughed so uncontrollably.</i>

<i>After me and your dad acknowledged the situation we were: stranded in an island, in the middle of a forest, we got to the basics, find food, water, make shelter - there was nothing nearby, and we couldn’t trace back to the cave - he never saw and I couldn’t remember the correct direction. And we couldn’t just leave the clearing easily. There are dangerous creatures in the woods. For some reason they don’t advance into the clearing. We don’t know why.</i>

#Engine Demo

Below there is a link to demo the engine as is. If you are on a computer, use WASD keys to walk and IJ keys to interact. You can also try it out with a Xbox360 joypad. If you are on a smartphone, there is touch support but the performance may vary depending on device power and resolution - the engine is still not very optimized.

It's only a demo, and there are still some bugs. The game should auto resize when necessary - like when switching to full-screen. If it doesn't, just press 0 on the keyboard. I plan on hiding the mouse and some more tweaks later on.

{% include image.html img="/images/title.png" title="click on this image to open the demo." url="/builds/build00001/index.html" %}

If you like the demo, have some comments, or else, hit me up on twitter.
