---
layout: post
title:  "A Glass of Lores"
date:   2015-02-07 04:34:25
categories: feature
tags: featured
image: /assets/article_images/2015-02-07-a_glass_of_lores/world_map.JPG
---
#In the beginning.
<i>My mom told me this story when I was very little, maybe some parts I modified, hearing others in the village. We came from the Unseeable Continent, where existed three small cities, each one with different focus, so near one from the other, with a plain plaza in the heart, where people gathered to discuss their dilemmas, drank for love, wrote poems, stories and pursued the reason of their own existence. Valleti produced wine, Villabassa focused on the rice crops, and  Montemitro bred the Yaks. Their texts traveled the world, and their readers got interested in these small cities.</i>

<i>One of this readers was an Emperor, from a far away land. That emperor was very young, and very impressed with the quality, and requested a visit to Trisan. The emperor visit was not well received by the citizens, who appreciated the isolation from the rest of the Unseeable Continent. The emperor came and went, and during his stay in the land, he noted, looking in the sand, a bright mineral, a very rare one, that he had not seen before in the vast amount as it was there available to the citizens of the three cities.</i>

<i>Unfortunately, not only did the people from Trisan knew no way of how to extract that mineral from the sand, they had no army, and no way to defend themselves. The emperor sent a messenger to deliver a big floured text that read as threat, “send the mineral or I will send my troops, and I will send my troops to bring your three cities to ashes in less than ten years”.</i>

<i>The people from Trisan decided they needed to be capable of defending itself, they looked the old texts on how to build weapons and started reading on how to fight a combat. A small explorer group was united to find the resources. The scouter group decided the near islands were safer places to explore, because it was known they were uninhabited.

<i>A big island turned out to be the perfect finding - a big mountain made almost solely of iron, so hot in places that it melted to the surface. Weapons could be forged there easily. The island had no fertile ground, and the water there only existed in small lakes that were created by rain. Since the mountain was very tall, clouds always stood over and the rain was certain even if not constant. To make use of Forge Island, the same ships that went to land to bring weapons had to keep returning providing food.</i>

<i>People on Forge Island became united on the task that brought them there. A Forge is a very unsuited place to be working straight, but work was all that was there, small tasks, like caring for the crops were not needed anymore, and discussing politics suddenly didn’t seem to matter. But they were very aware of their importance, and they felt like a big family.</i>

<i>Forge Island was unfortunately very open, had a deep sea and with no rocks to protect it from ships that could some day reach it in the imminent war. The people on Forge Island, which was by then a village already, decided to send a small group of people, in two ships, to the nearest and most rocky island, to build a fortress and a lighting post there to warn and defend the people on Forge Island.</i>

<i>This was a very risky thing to do because the nearest rocky island, that was so perfect to have a fort, was so good at it because it has very hard to successfully dock in. When the ships reached this island, was already night, and rain started to pour. The ships crashed near land, and started to sunken, people fled, swimming. Most survived. At the beach they gathered, with much enthusiasm for being alive. But that enthusiasm was soon to vanish.</i>

<i>You see, right after this gathering, which was at night, they turned their heads. Still a little deafened by the accident they didn’t realized that the rain in the island they now were was a storm in the island they come. Forge Island, that was mostly a volcano, erupted, and the night turned red, with white flashes from the distant lightnings.</i>

<i>That eruption lasted for days, we don’t know what happened after in the Unseeable Continent or if there were any survivors in Forge Island. What I do know is the people who survived the crash were too outnumbered to care, and with their knowledge and with what was salvaged from the ships they found this village. We do know we’ve made no contact with people outside this small island. And we did learn how to sharpen our remaining blades to defend from the monsters that inhabit this island with us.</i>
